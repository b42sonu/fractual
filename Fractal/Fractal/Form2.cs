﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fractal
{
    public partial class Form2 : Form
    {
        // Factor for zoom the image
        private float zoomFac = 1;
        //set Zoom allowed
        private bool zoomSet = false;

        //value for moving the image in X direction
        private float translateX = 0;
        //value for moving the image in Y direction
        private float translateY = 0;

        //Flag to set the moving operation set
        private bool translateSet = false;
        //Flag to set mouse down on the image
        private bool translate = false;

        //set on the mouse down to know from where moving starts
        private float transStartX;
        private float transStartY;

        //Current Image position after moving 
        private float curImageX = 0;
        private float curImageY = 0;


        //temporary storage in bitmap
        Image bmp;

        float ratio;
        float translateRatio;
        public Form2()
        {
            InitializeComponent();
            //set Zoom allowed
            zoomSet = true;

            //call the picture box paint
            //pictureBox.Refresh();
            pictureBox.Cursor = Cursors.Arrow;

            //moving operation unset
            //translateSet = false;




            string imagefilename = Application.StartupPath + "//Desert.jpg";
            bmp = Image.FromFile(imagefilename);

            Graphics g = this.CreateGraphics();

            //// Fit whole image
            //zoom = Math.Min(
            //    ((float)pictureBox.Height / (float)img.Height) * (img.VerticalResolution / g.DpiY),
            //    ((float)pictureBox.Width / (float)img.Width) * (img.HorizontalResolution / g.DpiX)
            //);

            // Fit width
            zoomFac = ((float)pictureBox.Width / (float)bmp.Width) * (bmp.HorizontalResolution / g.DpiX);

            // Check potrait or landscape
            if (bmp.Width > bmp.Height)
            {
                ratio = (float)pictureBox.Width / (float)bmp.Width;
                translateRatio = (float)bmp.Width / (float)pictureBox.Width;

            }
            else
            {
                ratio = (float)pictureBox.Height / (float)bmp.Height;
                translateRatio = (float)bmp.Height / (float)pictureBox.Height;

            }

            pictureBox.Paint += new PaintEventHandler(imageBox_Paint);
        }

        private void imageBox_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.ScaleTransform(zoomFac, zoomFac);//zoomFac, zoomFac);
            e.Graphics.DrawImage(bmp, 0, 0);
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {

            //mouse down is true
            translate = true;
            //starting coordinates for move
            transStartX = e.X;
            transStartY = e.Y;


        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            //If mouse down is true
            //if (translate == true)
            //{
            //    //calculate the total distance to move from 0,0
            //    //previous image position+ current moving distance
            //    translateX = curImageX + ((e.X - transStartX) * (translateRatio / zoomFac));
            //    translateY = curImageY + ((e.Y - transStartY) * (translateRatio / zoomFac));
            //    //call picturebox to update the image in the new position
            //    translateSet = true;
            //}

            pictureBox.Refresh();
            ////set mouse down operation end
            //translate = false;
            ////set present position of the image after move.
            //curImageX = translateX;
            //curImageY = translateY;

            zoomFac = (float) (zoomFac + 0.01);
        }
    }
}
